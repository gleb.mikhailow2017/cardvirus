﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropPlace : MonoBehaviour, IDropHandler
{
    public CardContrl card;
    public void OnDrop(PointerEventData eventData)
    {
        card = eventData.pointerDrag.GetComponent<CardContrl>();

        if (card)
        {
            card.defPar = transform;
        }
    }
    private void Update()
    {
        if(transform.childCount != 0)
        {
            GetComponent<Image>().raycastTarget = false;
        }
    }
}
