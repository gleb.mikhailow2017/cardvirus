﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardContrl : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public int at;
    public int hp;

    Text stats;

    Transform tmp;
    Transform hand;
    GameManager gm;

    Camera main;
    Vector3 offset;
    public Transform defPar;
    public void OnBeginDrag(PointerEventData eventData)
    {
        offset = transform.position- main.ScreenToWorldPoint(eventData.position);

        tmp = transform.parent;
        transform.SetParent(hand.parent);
        defPar = hand;

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 NewPos = main.ScreenToWorldPoint(eventData.position);
        NewPos.z = -1f;
        transform.position = NewPos + offset;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(defPar);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (defPar != tmp && defPar == hand) gm._energy++;
        if (defPar != tmp && defPar != hand) gm._energy--;
    }

    private void Update()
    {
        if (gm._energy<=0)
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        else
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (hp <= 0) Destroy(gameObject);
        if (gm._next)
        {
            gm._next = false;
            Attack(gameObject.tag);
        }
    }
    void Start()
    {
        if (gameObject.tag == "shl")
        {
            hp = Random.Range(3, 10);
            at = Random.Range(0, 3);
        }
        if (gameObject.tag == "att")
        {
            hp = Random.Range(2, 5);
            at = Random.Range(3, 9);
        }
        gm = GameObject.FindObjectOfType<GameManager>();

        if (gameObject.tag != "eff")
        {
            stats = GetComponentInChildren<Text>();
            stats.text = "hp: " + hp + "\n" + "att: " + at;
        }

        defPar = null;
        main = Camera.allCameras[0];
        hand = transform.parent;
    }

    public void Attack(string tg)
    {
        if (tg == "att")
        {
            if (GameObject.FindGameObjectWithTag("shl"))
            {
                GameObject s = GameObject.FindGameObjectWithTag("shl");
                if (s.layer == gameObject.layer)
                {
                    s.GetComponent<CardContrl>().hp -= at;
                    hp -= s.GetComponent<CardContrl>().at;
                }
                return;
            }
            if (GameObject.FindGameObjectWithTag("att"))
            {
                GameObject s = GameObject.FindGameObjectWithTag("att");
                if (s.layer == gameObject.layer)
                {
                    s.GetComponent<CardContrl>().hp -= at;
                    hp -= s.GetComponent<CardContrl>().at;
                }
            }
        }
    }
}
