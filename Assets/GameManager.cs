﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject[] cards;
    public Transform energy;
    public int _energy = 3;
    public bool _next = false;
    public void Next()
    {
        _energy = 3;
        NewCart();
        _next = true;
    }

    void NewCart()
    {
        GameObject b = cards[Random.Range(0, cards.Length - 1)];
        GameObject.Instantiate(b,GameObject.FindGameObjectWithTag("hand").transform);
    }

    private void Update()
    {
        for (int i=2; i >= 0; i--)
        {
            if (_energy < i+1)
            {
                energy.GetChild(i).gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < 3; i++)
        {
            if (_energy > i)
            {
                energy.GetChild(i).gameObject.SetActive(true);
            }
        }
    }
}
